<?php

namespace App\Controller;

use App\Entity\Films;
use App\Form\FilmType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class FilmsControler extends AbstractController
{

    #[Route("/", name: "home")]
    public function home(): Response
    {
        return $this -> render('views/homeFilms.html.twig');
    }

    #[Route("/peliculas", name: "listFilms")]
    public function getFilms(EntityManagerInterface $doctrine)
    {
        $repository = $doctrine -> getRepository(Films::class);
        $films = $repository -> findAll();
        return $this -> render('views/listFilms.html.twig', ["films" => $films]);
    }

    #[Route("/pelicula/{id}", name: "getFilm")]
    public function getFilm(EntityManagerInterface $doctrine, $id)
    {
        $repository = $doctrine -> getRepository(Films::class);
        $film = $repository -> findOneBy(["id" => $id]);
        return $this -> render('views/getFilm.html.twig', ["film" => $film]);

    }

    #[Route("/nueva", name: "newFilm")]
    public function newFilm(Request $request, EntityManagerInterface $doctrine)
    {
        $form = $this -> createForm(FilmType::class);
        $form -> handleRequest($request);
        if($form -> isSubmitted() && $form -> isValid()) {
            $film = $form -> getData();
            $doctrine -> persist($film);
            $doctrine -> flush();
        }
        return $this -> renderForm('views/formFilm.html.twig', ["formFilm" => $form]);
    }

    #[Route("/edit/film/{id}", name: "editFilm")]
    public function editFilm(Request $request, EntityManagerInterface $doctrine, $id)
    {
        $repository = $doctrine -> getRepository(Films::class);
        $findFilm = $repository -> find($id);
        $form = $this -> createForm(FilmType::class, $findFilm);
        $form -> handleRequest($request);
        if($form -> isSubmitted() && $form -> isValid()) {
            $film = $form -> getData();
            $doctrine -> persist($film);
            $doctrine -> flush();
            return $this->redirectToRoute("listFilms");
        }
        return $this -> renderForm('views/formFilm.html.twig', ["formFilm" => $form]);
    }









}