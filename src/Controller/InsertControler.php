<?php

namespace App\Controller;

use App\Entity\Films;
use App\Entity\Genres;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InsertControler extends AbstractController
{
    #[Route("/insert/films")]
    public function insertFilms(EntityManagerInterface $doctrine)
    {
        $genre1 = new Genres();
        $genre1 -> setName("Comedia");
        $genre1 -> setImg("https://concepto.de/wp-content/uploads/2018/09/comedia-e1536069171944.jpg");


        $film1 = new Films();
        $film1 -> setName("Regreso al futuro");
        $film1 -> setImg("https://images-eu.ssl-images-amazon.com/images/S/pv-target-images/8d89760e9d841d13b106242d5aa53f020871c854d744178f7c2cc81f0c4b21c6._UR1920,1080_RI_UX400_UY225_.jpg");
        $film1 -> addGenre($genre1);


        $doctrine -> persist($genre1);
        $doctrine -> persist($film1);

        $doctrine -> flush();
        return new Response("<h1>Peliculas y Genero insertadas correctamente</h1>");

    }
}