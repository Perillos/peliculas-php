<?php

namespace App\Controller;

use App\Entity\Genres;
use App\Form\GenreType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GenreControler extends AbstractController
{

    #[Route("/genero", name: "listGenres")]
    public function getGenres(EntityManagerInterface $doctrine)
    {
        $repository = $doctrine -> getRepository(Genres::class);
        $genres = $repository -> findAll();
        return $this -> render('views/listGenres.html.twig', ["genres" => $genres]);
    }

    #[Route("/genero/{id}", name: "getGenre")]
    public function getGenre(EntityManagerInterface $doctrine, $id)
    {
        $repository = $doctrine -> getRepository(Genres::class);
        $genre = $repository -> findOneBy(["id" => $id]);
        return $this -> render('views/getGenre.html.twig', ["genre" => $genre]);

    }

    #[Route("/nuevo", name: "newGenre")]
    public function newGenre(Request $request, EntityManagerInterface $doctrine)
    {
        $form = $this -> createForm(GenreType::class);
        $form -> handleRequest($request);
        if($form -> isSubmitted() && $form -> isValid()) {
            $genre = $form -> getData();
            $doctrine -> persist($genre);
            $doctrine -> flush();
        }
        return $this -> renderForm('views/formGenre.html.twig', ["formGenre" => $form]);
    }

    #[Route("/edit/genre/{id}", name: "editGenre")]
    public function editFilm(Request $request, EntityManagerInterface $doctrine, $id)
    {
        $repository = $doctrine -> getRepository(Genres::class);
        $findGenre = $repository -> find($id);
        $form = $this -> createForm(GenreType::class, $findGenre);
        $form -> handleRequest($request);
        if($form -> isSubmitted() && $form -> isValid()) {
            $genre = $form -> getData();
            $doctrine -> persist($genre);
            $doctrine -> flush();
            return $this->redirectToRoute("listGenres");
        }
        return $this -> renderForm('views/formGenre.html.twig', ["formGenre" => $form]);
    }


}